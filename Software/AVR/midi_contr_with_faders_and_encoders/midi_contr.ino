
#include <LiquidCrystal.h>


const int rs = 7, en = 6, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

int Faders_Value[] =          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};       
int Faders_last_Value[] =     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

  
unsigned int Encoders_State[] =          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};       
unsigned int Encoders_last_State[] =     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
unsigned int Encoders_Counter[] =        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; 
 
unsigned int Bank1_sw_PushCounter =        0; 
unsigned int Bank1_sw_State =              0;       
unsigned int Bank1_sw_last_State =         0; 
  
unsigned int Bank2_sw_PushCounter =        0; 
unsigned int Bank2_sw_State =              0;       
unsigned int Bank2_sw_last_State =         0;   

unsigned int MIDI_ch_up_sw_PushCounter =   0; 
unsigned int MIDI_ch_up_sw_State =         0;       
unsigned int MIDI_ch_up_sw_last_State =    0;   

unsigned int MIDI_ch_down_sw_PushCounter = 0; 
unsigned int MIDI_ch_down_sw_State =       0;       
unsigned int MIDI_ch_down_sw_last_State =  0;   



const int Faders[] = {A0, A1, A2, A3, A4, A5, A6, A7, A8, A0};

const int Encoders_down[] =   {22, 24, 26, 28, 30, 32, 35, 36, 38, 40};
const int Encoders_up[] =     {23, 25, 27, 29, 31, 33, 34, 37, 39, 41};

const int Bank1_sw = 45;
const int Bank1_led = 43;

const int Bank2_sw = 44;
const int Bank2_led = 42; 

const int MIDI_ch_up_sw = 10;
const int MIDI_ch_up_led = 8; 

const int MIDI_ch_down_sw = 11;
const int MIDI_ch_down_led = 9; 

void setup() {

pinMode(Encoders_up[0], INPUT_PULLUP);
pinMode(Encoders_up[1], INPUT_PULLUP);
pinMode(Encoders_up[2], INPUT_PULLUP);
pinMode(Encoders_up[3], INPUT_PULLUP);
pinMode(Encoders_up[4], INPUT_PULLUP);
pinMode(Encoders_up[5], INPUT_PULLUP);
pinMode(Encoders_up[6], INPUT_PULLUP);
pinMode(Encoders_up[7], INPUT_PULLUP);
pinMode(Encoders_up[8], INPUT_PULLUP);
pinMode(Encoders_up[9], INPUT_PULLUP);

pinMode(Encoders_down[0], INPUT_PULLUP);
pinMode(Encoders_down[1], INPUT_PULLUP);
pinMode(Encoders_down[2], INPUT_PULLUP);
pinMode(Encoders_down[3], INPUT_PULLUP);
pinMode(Encoders_down[4], INPUT_PULLUP);
pinMode(Encoders_down[5], INPUT_PULLUP);
pinMode(Encoders_down[6], INPUT_PULLUP);
pinMode(Encoders_down[7], INPUT_PULLUP);
pinMode(Encoders_down[8], INPUT_PULLUP);
pinMode(Encoders_down[9], INPUT_PULLUP);


pinMode(Bank1_sw,        INPUT_PULLUP);
pinMode(Bank2_sw,        INPUT_PULLUP);
pinMode(MIDI_ch_up_sw,   INPUT_PULLUP);
pinMode(MIDI_ch_down_sw, INPUT_PULLUP);

pinMode(Bank1_led,        OUTPUT);
pinMode(Bank2_led,        OUTPUT);
pinMode(MIDI_ch_up_led,   OUTPUT);
pinMode(MIDI_ch_down_led, OUTPUT);
        
lcd.begin(16, 2);
lcd.print("hello, world!");

}

void loop()
{
/*if (digitalRead(Bank1_sw) == LOW) 
    {
     digitalWrite(Bank1_led, HIGH);
     delay(50);
     digitalWrite(Bank1_led, LOW);
    } 
if (digitalRead(Bank2_sw) == LOW) 
    {
     digitalWrite(Bank2_led, HIGH);
     delay(50);
     digitalWrite(Bank2_led, LOW);
    } 

   
if (digitalRead(MIDI_ch_up_sw) == LOW) 
    {
     digitalWrite(MIDI_ch_up_led, HIGH);
     delay(50);
     digitalWrite(MIDI_ch_up_led, LOW);
    }     
if (digitalRead(MIDI_ch_down_sw) == LOW) 
    {
     digitalWrite(MIDI_ch_down_led, HIGH);
     delay(50);
     digitalWrite(MIDI_ch_down_led, LOW);
    }    
*/
  
for (int b = 0; b < 10; b++)
 {
    Faders_read(b);  
    Encoders_read(b);
 } 
}



void Encoders_read(int i) 
{
  Encoders_State[i] = digitalRead(Encoders_up[i]);
  if ((Encoders_last_State[i] == LOW) && (Encoders_State[i] == HIGH)) 
  {
    if (digitalRead(Encoders_down[i]) == LOW) 
    {
      Encoders_Counter[i]--;
      lcd.clear();
      lcd.setCursor(0, 0); lcd.write("P:");
      lcd.setCursor(3, 0); lcd.print(i+1);
      lcd.setCursor(6, 0); lcd.print(Encoders_Counter[i]);
      delay(5);
    } 
    else
    {
      Encoders_Counter[i]++;
      lcd.clear();
      lcd.setCursor(0, 0); lcd.write("P:");
      lcd.setCursor(3, 0); lcd.print(i+1);
      lcd.setCursor(6, 0); lcd.print(Encoders_Counter[i]);
      delay(5);
    }
  }
  Encoders_last_State[i] = Encoders_State[i];
}




void Faders_read(int i) 
{
  
Faders_Value[i] = analogRead(Faders[i]);

  if (Faders_Value[i] != Faders_last_Value[i] && abs(Faders_Value[i] - Faders_last_Value[i]) > 12)
  {
    lcd.clear();
    lcd.setCursor(0, 0); lcd.write("Fader:");
    lcd.setCursor(6, 0); lcd.print(i+1);
    lcd.setCursor(7, 0); lcd.write("  ");
    lcd.setCursor(9, 0); lcd.print(Faders_Value[i]/10);
  }
  Faders_last_Value[i] = Faders_Value[i]; 
  delay(2);
}




int pinState = LOW;
unsigned long previousMillis = 0;
const long interval = 10;


const int row1 = 37;
const int row2 = 35;
const int row3 = 33;
const int row4 = 31;
const int row5 = 29;
const int row6 = 27;
const int row7 = 25;
const int row8 = 23;

const int colums[] = {36, 34, 32, 30, 28, 26, 24, 22};

uint8_t keyToMidiMap[64];

boolean keyPressed[64];

int noteVelocity = 127;



void scanColumn(int value) {
/*  delay(2);
  digitalWrite(colums[value], HIGH); 
  digitalWrite(colums[value], LOW);
*/
 unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;
    if (pinState == LOW) {
      pinState = HIGH;
    } else {
      pinState = LOW;
    }

    digitalWrite(colums[value], pinState);
  }


  
}

void setup() {
  
  // Map scan matrix buttons/keys to actual Midi note number. Lowest num 41 corresponds to F MIDI note.
  keyToMidiMap[0] = 48;
  keyToMidiMap[1] = 41;
  keyToMidiMap[2] = 42;
  keyToMidiMap[3] = 43;
  keyToMidiMap[4] = 44;
  keyToMidiMap[5] = 45;
  keyToMidiMap[6] = 46;
  keyToMidiMap[7] = 47;

  keyToMidiMap[8] = 56;
  keyToMidiMap[1 + 8] = 49;
  keyToMidiMap[2 + 8] = 50;
  keyToMidiMap[3 + 8] = 51;
  keyToMidiMap[4 + 8] = 52;
  keyToMidiMap[5 + 8] = 53;
  keyToMidiMap[6 + 8] = 54;
  keyToMidiMap[7 + 8] = 55;

  keyToMidiMap[16] = 64;
  keyToMidiMap[1 + 16] = 57;
  keyToMidiMap[2 + 16] = 58;
  keyToMidiMap[3 + 16] = 59;
  keyToMidiMap[4 + 16] = 60;
  keyToMidiMap[5 + 16] = 61;
  keyToMidiMap[6 + 16] = 62;
  keyToMidiMap[7 + 16] = 63;

  keyToMidiMap[24] = 72;
  keyToMidiMap[1 + 24] = 65;
  keyToMidiMap[2 + 24] = 66;
  keyToMidiMap[3 + 24] = 67;
  keyToMidiMap[4 + 24] = 68;
  keyToMidiMap[5 + 24] = 69;
  keyToMidiMap[6 + 24] = 70;
  keyToMidiMap[7 + 24] = 71;

  keyToMidiMap[32] = 80;
  keyToMidiMap[1 + 32] = 73;
  keyToMidiMap[2 + 32] = 74;
  keyToMidiMap[3 + 32] = 75;
  keyToMidiMap[4 + 32] = 76;
  keyToMidiMap[5 + 32] = 77;
  keyToMidiMap[6 + 32] = 78;
  keyToMidiMap[7 + 32] = 79;

  keyToMidiMap[40] = 88;
  keyToMidiMap[1 + 40] = 81;
  keyToMidiMap[2 + 40] = 82;
  keyToMidiMap[3 + 40] = 83;
  keyToMidiMap[4 + 40] = 84;
  keyToMidiMap[5 + 40] = 85;
  keyToMidiMap[6 + 40] = 86;
  keyToMidiMap[7 + 40] = 87;

   keyToMidiMap[48] = 96;
  keyToMidiMap[1 + 48] = 89;
  keyToMidiMap[2 + 48] = 90;
  keyToMidiMap[3 + 48] = 91;
  keyToMidiMap[4 + 48] = 92;
  keyToMidiMap[5 + 48] = 93;
  keyToMidiMap[6 + 48] = 94;
  keyToMidiMap[7 + 48] = 95;

  keyToMidiMap[56] = 104;
  keyToMidiMap[1 + 56] = 97;
  keyToMidiMap[2 + 56] = 98;
  keyToMidiMap[3 + 56] = 99;
  keyToMidiMap[4 + 56] = 100;
  keyToMidiMap[5 + 56] = 101;
  keyToMidiMap[6 + 56] = 102;
  keyToMidiMap[7 + 56] = 103;

  
  
  // setup pins output/input mode
  pinMode(colums[0], OUTPUT);
  pinMode(colums[1], OUTPUT);
  pinMode(colums[2], OUTPUT);
  pinMode(colums[3], OUTPUT);
  pinMode(colums[4], OUTPUT);
  pinMode(colums[5], OUTPUT);
  pinMode(colums[6], OUTPUT);
  pinMode(colums[7], OUTPUT);

  pinMode(row1, INPUT);
  pinMode(row2, INPUT);
  pinMode(row3, INPUT);
  pinMode(row4, INPUT);
  pinMode(row5, INPUT);
  pinMode(row6, INPUT);
  pinMode(row7, INPUT);
  pinMode(row8, INPUT);

    Serial.begin(57600);

  delay(1000);

}

void loop() {

  for (int col = 0; col < 8; col++) {
    
 
    scanColumn(col); 
    
    int groupValue1 = digitalRead(row1);
    int groupValue2 = digitalRead(row2);
    int groupValue3 = digitalRead(row3);
   /* int groupValue4 = digitalRead(row4);
    int groupValue5 = digitalRead(row5);
    int groupValue6 = digitalRead(row6);
    int groupValue7 = digitalRead(row7);
    int groupValue8 = digitalRead(row8);
*/
    // process if any combination of keys pressed
    if (groupValue1 == 1 || groupValue2 == 1 || groupValue3 == 1
       /* || groupValue4 != 0 || groupValue5 != 0 || groupValue6 != 0 || groupValue7 != 0 || groupValue8 != 0*/) {

      if (groupValue1 == 1 && !keyPressed[col]) {
        keyPressed[col] = true;
        noteOn(0x91, keyToMidiMap[col], noteVelocity);
      }

      if (groupValue2 == 1 && !keyPressed[col + 8]) {
        keyPressed[col + 8] = true;
        noteOn(0x91, keyToMidiMap[col + 8], noteVelocity);
      }

      if (groupValue3 == 1 && !keyPressed[col + 16]) {
        keyPressed[col + 16] = true;
        noteOn(0x91, keyToMidiMap[col + 16], noteVelocity);
      }
/*
      if (groupValue4 != 0 && !keyPressed[col + 24]) {
        keyPressed[col + 24] = true;
        noteOn(0x91, keyToMidiMap[col + 24], noteVelocity);
      }
      
      if (groupValue5 != 0 && !keyPressed[col + 32]) {
        keyPressed[col + 32] = true;
        noteOn(0x91, keyToMidiMap[col + 32], noteVelocity);
      }
      
      if (groupValue6 != 0 && !keyPressed[col + 40]) {
        keyPressed[col + 40] = true;
        noteOn(0x91, keyToMidiMap[col + 40], noteVelocity);
      }
      
      if (groupValue7 != 0 && !keyPressed[col + 48]) {
        keyPressed[col + 48] = true;
        noteOn(0x91, keyToMidiMap[col + 48], noteVelocity);
      }
      
      if (groupValue8 != 0 && !keyPressed[col + 56]) {
        keyPressed[col + 56] = true;
        noteOn(0x91, keyToMidiMap[col + 56], noteVelocity);
      }
*/
    }

    //  process if any combination of keys released
    if (groupValue1 == 0 && keyPressed[col]) {
      keyPressed[col] = false;
      noteOn(0x91, keyToMidiMap[col], 0);
    }

    if (groupValue2 == 0 && keyPressed[col + 8]) {
      keyPressed[col + 8] = false;
      noteOn(0x91, keyToMidiMap[col + 8], 0);
    }

    if (groupValue3 == 0 && keyPressed[col + 16]) {
      keyPressed[col + 16] = false;
      noteOn(0x91, keyToMidiMap[col + 16], 0);
    }
/*
    if (groupValue4 == 0 && keyPressed[col + 24]) {
      keyPressed[col + 24] = false;
      noteOn(0x91, keyToMidiMap[col + 24], 0);
    }
    
    if (groupValue5 == 0 && keyPressed[col + 32]) {
      keyPressed[col + 32] = false;
      noteOn(0x91, keyToMidiMap[col + 32], 0);
    }
    
    if (groupValue6 == 0 && keyPressed[col + 40]) {
      keyPressed[col + 40] = false;
      noteOn(0x91, keyToMidiMap[col + 40], 0);
    }
    
    if (groupValue7 == 0 && keyPressed[col + 48]) {
      keyPressed[col + 48] = false;
      noteOn(0x91, keyToMidiMap[col + 48], 0);
    }
    
    if (groupValue8 == 0 && keyPressed[col + 56]) {
      keyPressed[col + 56] = false;
      noteOn(0x91, keyToMidiMap[col + 56], 0);
    }
*/
  }

}


void noteOn(int cmd, int pitch, int velocity) {
  Serial.print(cmd);   Serial.print(":");
  Serial.print(pitch);   Serial.print(":");
  Serial.println(velocity);
}

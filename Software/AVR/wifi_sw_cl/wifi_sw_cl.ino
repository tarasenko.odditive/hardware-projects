#include <ESP8266WiFi.h>

char ssid[] = "NordicResults";    
char pass[] = "CYD!23Q?1Kr**_";   
 
WiFiServer server(80);
 
String header = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n";
String html_1 = "<!DOCTYPE html><html><head><meta name='viewport' content='width=device-width, initial-scale=1.0'/><meta charset='utf-8'><style>body {font-size:140%;} #main {display: table; margin: auto;  padding: 0 10px 0 10px; } h2,{text-align:center; } .button { padding:10px 10px 10px 10px; width:100%;  background-color: #4CAF50; font-size: 120%;}</style><title>Load Control</title></head><body><div id='main'><h2>Load Control</h2>";
String html_2 = "";
String html_21 = "";
String html_22 = "";
String html_23 = "";
String html_4 = "</div></body></html>";
String html_5 = "";
 
String request = "";


int LED_Pin = 5;
int LED_conn_Pin = 4;

int status = WL_IDLE_STATUS;
byte mac[6];



 
void setup() 
{
      pinMode(LED_Pin, OUTPUT); 
      pinMode(LED_conn_Pin, OUTPUT); 
      digitalWrite(LED_conn_Pin, LOW);
 
      Serial.begin(9600);
      delay(500);
      Serial.println(F("Serial started at 9600"));
      Serial.println();

    
      // We start by connecting to a WiFi network
      Serial.print(F("Connecting to "));  Serial.println(ssid);
      WiFi.begin(ssid, pass);

      while (WiFi.status() != WL_CONNECTED) 
      {
          Serial.print(".");    delay(500);
      }
      Serial.println("");
      Serial.println(F("[CONNECTED]"));
      Serial.print("[IP ");              
      Serial.print(WiFi.localIP()); 
      Serial.println("]");
      Serial.println("---------------------------------------");
        // Print your MAC address:
      WiFi.macAddress(mac);
      Serial.print("MAC: ");
      Serial.print(mac[5],HEX);
      Serial.print(":");
      Serial.print(mac[4],HEX);
      Serial.print(":");
      Serial.print(mac[3],HEX);
      Serial.print(":");
      Serial.print(mac[2],HEX);
      Serial.print(":");
      Serial.print(mac[1],HEX);
      Serial.print(":");
      Serial.println(mac[0],HEX);
      
        

      // start a server
      server.begin();
      Serial.println("Server started");
      Serial.println("---------------------------------------");
 
} // void setup()
 
 
 
void loop() 
{
 
    // Check if a client has connected
    WiFiClient client = server.available();
    if (!client)  {  return;  digitalWrite(LED_conn_Pin, LOW); }
    
    digitalWrite(LED_conn_Pin, HIGH);
    // Read the first line of the request
    request = client.readStringUntil('\r');
 
    if       ( request.indexOf("SWON") > 0 )  { digitalWrite(LED_Pin, HIGH);  }
    else if  ( request.indexOf("SWOFF") > 0 ) { digitalWrite(LED_Pin, LOW);   }
 

 
    // Get the LED pin status and create the LED status message
          int status = WL_IDLE_STATUS;
          //byte mac[6];
          char buffer [256];
          char macadr [256];
          int n;
          int f;
          n=sprintf (buffer,"%02X%02X%02X%02X%02X%02X",mac[5],mac[4],mac[3],mac[2],mac[1],mac[0]); //"%d"
          f=sprintf (macadr,"%02X%02X%02X%02X%02X%02X",mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]); //"%d"
          Serial.println(buffer);

          
    if (digitalRead(LED_Pin) == HIGH) 
    {
        // the LED is on so the button needs to say turn it off
      html_21 = "<form id='F1' action='SWOFF'><input class='button' type='submit' value='OFF:";
      html_23 = "' ></form><br>"; 

     client.flush();
 
    client.print( header );
    client.print( html_1 );    
    client.print( html_21 );
    client.print( buffer );
    client.print( html_23 );
    client.print( html_4);
        delay(5);
    }
    else                              
    {
        // the LED is off so the button needs to say turn it on
      // html_2 = "<form id='F1' action='SWON'><input class='button' type='submit' value='Turn on the load' ></form><br>";
      html_21 = "<form id='F1' action='SWON'><input class='button' type='submit' value='ON:";
      html_23 = "' ></form><br>"; 
  
      
    client.flush();
 
    client.print( header );
    client.print( html_1 );    
    client.print( html_21 );           
    client.print( "MAC" );
    client.print( html_23 );
    client.print( html_4);
        delay(5);
    }
 
  
 

  // The client will actually be disconnected when the function returns and 'client' object is detroyed
 
} // void loop()



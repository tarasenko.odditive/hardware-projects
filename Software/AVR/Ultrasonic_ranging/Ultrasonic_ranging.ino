int trigPin = 10; 
int echoPin = 11;  
int fanPin = 9;  

int sensorValue = 0;        
int sensorMin = 300;        
int sensorMax = 3000;          

void setup() { 
  Serial.begin (9600); 
  pinMode(trigPin, OUTPUT); 
  pinMode(echoPin, INPUT); 


  while (millis() < 5000) 
  {
  digitalWrite(trigPin, LOW); 
  delayMicroseconds(2); 
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10); 
  digitalWrite(trigPin, LOW); 
  sensorValue = pulseIn(echoPin, HIGH); 
  }

  
} 
 
void loop() { 

  digitalWrite(trigPin, LOW); 
  delayMicroseconds(2); 
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10); 
  digitalWrite(trigPin, LOW); 
  sensorValue = pulseIn(echoPin, HIGH);
  
  //Serial.println(sensorValue); 
  //delay(100);
 
  sensorValue = map(sensorValue, sensorMin, sensorMax, 0, 255);
  sensorValue = constrain(sensorValue, 170, 255);
  analogWrite(fanPin, sensorValue);

 
}

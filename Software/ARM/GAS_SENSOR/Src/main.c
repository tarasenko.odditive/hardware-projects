/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l0xx_hal.h"


/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc;

//IWDG_HandleTypeDef hiwdg;

LPTIM_HandleTypeDef hlptim1;

UART_HandleTypeDef huart1;


/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC_Init(void);
static void MX_USART1_UART_Init(void);
//static void MX_IWDG_Init(void);
static void MX_LPTIM1_Init(void);

void enter_Run(void);
void enter_LPRun(void);
void enter_Sleep(void);
void Delay(uint32_t Dely);
//void v_bat();

uint8_t strUART_DEV_ID[]=         "FFFFFFFF:";
uint8_t str_USART_bat_level[]=    "BAT_LEVEL-";
uint8_t str_USART_sensor_event[]= "SENS_WARN-";
uint8_t str_USART_new_line[]=     "\r\n";


uint8_t S_BT_setUP_str1[]="AT+DEFAULT";
uint8_t S_BT_setUP_str2[]="AT+NAME=OGS"; 
uint8_t S_BT_setUP_str3[]="AT+ADDR=50658386B2C5";
 
 
char strADC[8]={0};

uint16_t sensor_ADC_CH;
uint16_t sensor_ADC_cycle1_CH;
uint16_t sensor_ADC_cycle2_CH;
uint16_t sensor_ADC_cycle3_CH;
uint16_t sensor_ADC_cycle4_CH;

uint16_t bat_ADC_CH;
uint16_t previous_bat_level;
uint32_t cycle_counter_LP;

uint16_t threshold= 3500;



int main(void)
{

 
  HAL_Init();

  
  SystemClock_Config();

  
  MX_GPIO_Init();
  MX_ADC_Init();
  MX_USART1_UART_Init();
  MX_LPTIM1_Init();
	
	
			 /*----------------------BT setup---------------------------------------------
	
		    GPIO_InitTypeDef GPIO_InitStruct;
		    HAL_GPIO_WritePin(GPIOA, BTEN_Pin , GPIO_PIN_SET);               
	    	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
		  	HAL_Delay(1000);
			  HAL_UART_Transmit(&huart1,S_BT_setUP_str3,32,0xFFFF);
	    	HAL_Delay(1000);
	
	     */
 
 while (1)
  {
	while(HAL_GPIO_ReadPin(GPIOA, USBIN_Pin)==GPIO_PIN_SET)     //for USB plugged in case 
	{
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	PWR->CR &= ~( PWR_CR_LPRUN );
	PWR->CR &= ~( PWR_CR_LPSDSR );
  
  SystemClock_Config();
  SystemCoreClockUpdate();

  MX_GPIO_Init();
  MX_ADC_Init();
  MX_USART1_UART_Init();
		
				
		
	
			
  /*----------------------Gas sensor power on---------------------------------------------*/	
        GPIO_InitTypeDef GPIO_InitStruct;
		    HAL_GPIO_WritePin(GPIOA, SESEN_Pin, GPIO_PIN_SET);               
	    	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
		  	HAL_Delay(1000);
			
  /*------------------ADC CH2 selection for sensor maesurement----------------------------*/		
		    ADC_ChannelConfTypeDef sConfig;
	  	  sConfig.Channel = ADC_CHANNEL_2; 
	      sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;   		
		    HAL_ADC_ConfigChannel(&hadc,  &sConfig);			
			
			  sensor_ADC_CH=0;
			
        sensor_ADC_cycle1_CH = 0; 
        HAL_ADC_Start(&hadc);
        HAL_ADC_PollForConversion(&hadc,100);
        sensor_ADC_cycle1_CH = (uint32_t) HAL_ADC_GetValue(&hadc);
	   	  HAL_ADC_Stop(&hadc);
		    HAL_Delay(100);
			
			  sensor_ADC_cycle2_CH = 0; 
        HAL_ADC_Start(&hadc);
        HAL_ADC_PollForConversion(&hadc,100);
        sensor_ADC_cycle2_CH = (uint32_t) HAL_ADC_GetValue(&hadc);
	   	  HAL_ADC_Stop(&hadc);
		    HAL_Delay(100);
			
		  	sensor_ADC_cycle3_CH = 0; 
        HAL_ADC_Start(&hadc);
        HAL_ADC_PollForConversion(&hadc,100);
        sensor_ADC_cycle3_CH = (uint32_t) HAL_ADC_GetValue(&hadc);
	   	  HAL_ADC_Stop(&hadc);
		    HAL_Delay(100);
				
				sensor_ADC_cycle4_CH = 0; 
        HAL_ADC_Start(&hadc);
        HAL_ADC_PollForConversion(&hadc,100);
        sensor_ADC_cycle4_CH = (uint32_t) HAL_ADC_GetValue(&hadc);
	   	  HAL_ADC_Stop(&hadc);
		    HAL_Delay(100);
			
	    	sensor_ADC_CH=(sensor_ADC_cycle1_CH+sensor_ADC_cycle2_CH+sensor_ADC_cycle3_CH+sensor_ADC_cycle4_CH)/4;		
				
			  if(sensor_ADC_CH>threshold)
				{
				/*----------------------BT power on---------------------------------------------*/	
	      GPIO_InitTypeDef GPIO_InitStruct;
		    HAL_GPIO_WritePin(GPIOA, BTEN_Pin , GPIO_PIN_SET);               
	    	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
		  	HAL_Delay(500);
					
					 while(sensor_ADC_CH>threshold)
							{
								
								sensor_ADC_CH = 0;
						    HAL_ADC_Start(&hadc);
                HAL_ADC_PollForConversion(&hadc,100);
                sensor_ADC_CH = (uint32_t) HAL_ADC_GetValue(&hadc);
	   	          HAL_ADC_Stop(&hadc);
								HAL_Delay(100);
								
								sprintf(strADC,"%04d",sensor_ADC_CH);
								
								HAL_UART_Transmit(&huart1,strUART_DEV_ID,10,0xFFFF);
			          HAL_UART_Transmit(&huart1,str_USART_sensor_event,10,0xFFFF);	  	
		            HAL_UART_Transmit(&huart1,strADC,8,0xFFFF);
	              HAL_UART_Transmit(&huart1,str_USART_new_line,2,0xFFFF);
	    	        HAL_Delay(300);

							}
							HAL_GPIO_WritePin(GPIOA, BTEN_Pin | SESEN_Pin , GPIO_PIN_RESET);               
            	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
				}
				else
				{
					HAL_GPIO_WritePin(GPIOA, BTEN_Pin | SESEN_Pin , GPIO_PIN_RESET);               
        	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
					HAL_Delay(3000);
				}
				
	}
	
			 
  while(HAL_GPIO_ReadPin(GPIOA, USBIN_Pin)==GPIO_PIN_RESET)     //for BT power mode 
	{
		
 
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	PWR->CR &= ~( PWR_CR_LPRUN );
	PWR->CR &= ~( PWR_CR_LPSDSR );
  
  SystemClock_Config();
  SystemCoreClockUpdate();

  MX_GPIO_Init();
  MX_ADC_Init();
  MX_USART1_UART_Init();
		
		
		//void v_bat();
	//	HAL_Delay(500);
	//	void v_bat();
	//	HAL_Delay(500);
		
		
  /*----------------------Gas sensor power on---------------------------------------------*/	
	      GPIO_InitTypeDef GPIO_InitStruct;
		    HAL_GPIO_WritePin(GPIOA, SESEN_Pin, GPIO_PIN_SET);               
	    	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
		  	HAL_Delay(1000);
			
  /*------------------ADC CH2 selection for sensor maesurement----------------------------*/		
		    ADC_ChannelConfTypeDef sConfig;
	  	  sConfig.Channel = ADC_CHANNEL_2; 
	      sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;   		
		    HAL_ADC_ConfigChannel(&hadc,  &sConfig);			
			
			  sensor_ADC_CH=0;
			
        sensor_ADC_cycle1_CH = 0; 
        HAL_ADC_Start(&hadc);
        HAL_ADC_PollForConversion(&hadc,100);
        sensor_ADC_cycle1_CH = (uint32_t) HAL_ADC_GetValue(&hadc);
	   	  HAL_ADC_Stop(&hadc);
		    HAL_Delay(100);
			
			  sensor_ADC_cycle2_CH = 0; 
        HAL_ADC_Start(&hadc);
        HAL_ADC_PollForConversion(&hadc,100);
        sensor_ADC_cycle2_CH = (uint32_t) HAL_ADC_GetValue(&hadc);
	   	  HAL_ADC_Stop(&hadc);
		    HAL_Delay(100);
			
		  	sensor_ADC_cycle3_CH = 0; 
        HAL_ADC_Start(&hadc);
        HAL_ADC_PollForConversion(&hadc,100);
        sensor_ADC_cycle3_CH = (uint32_t) HAL_ADC_GetValue(&hadc);
	   	  HAL_ADC_Stop(&hadc);
		    HAL_Delay(100);
				
				sensor_ADC_cycle4_CH = 0; 
        HAL_ADC_Start(&hadc);
        HAL_ADC_PollForConversion(&hadc,100);
        sensor_ADC_cycle4_CH = (uint32_t) HAL_ADC_GetValue(&hadc);
	   	  HAL_ADC_Stop(&hadc);
		    HAL_Delay(100);
			
	    	sensor_ADC_CH=(sensor_ADC_cycle1_CH+sensor_ADC_cycle2_CH+sensor_ADC_cycle3_CH+sensor_ADC_cycle4_CH)/4;		
				
			  if(sensor_ADC_CH>threshold)
				{
				/*----------------------BT power on---------------------------------------------*/	
	      GPIO_InitTypeDef GPIO_InitStruct;
		    HAL_GPIO_WritePin(GPIOA, BTEN_Pin , GPIO_PIN_SET);               
	    	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
		  	HAL_Delay(500);
					
					 while(sensor_ADC_CH>threshold)
							{
								
								sensor_ADC_CH = 0;
						    HAL_ADC_Start(&hadc);
                HAL_ADC_PollForConversion(&hadc,100);
                sensor_ADC_CH = (uint32_t) HAL_ADC_GetValue(&hadc);
	   	          HAL_ADC_Stop(&hadc);
								HAL_Delay(100);
								
								sprintf(strADC,"%04d",sensor_ADC_CH);
								
								HAL_UART_Transmit(&huart1,strUART_DEV_ID,10,0xFFFF);
			          HAL_UART_Transmit(&huart1,str_USART_sensor_event,10,0xFFFF);	  	
		            HAL_UART_Transmit(&huart1,strADC,8,0xFFFF);
	              HAL_UART_Transmit(&huart1,str_USART_new_line,2,0xFFFF);
	    	        HAL_Delay(300);

							}
							HAL_GPIO_WritePin(GPIOA, BTEN_Pin | SESEN_Pin , GPIO_PIN_RESET);               
            	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
							enter_LPRun();
				    	HAL_Delay(100);
				}
				else
				{
					HAL_GPIO_WritePin(GPIOA, BTEN_Pin | SESEN_Pin , GPIO_PIN_RESET);               
        	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
					enter_LPRun();
					HAL_Delay(100);
				}
				
	}
		
  }//while scope
 
}//int main scope

	
		

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_LPTIM1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_HSI;
  PeriphClkInit.LptimClockSelection = RCC_LPTIM1CLKSOURCE_LSI;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}


static void MX_ADC_Init(void)
{

  ADC_ChannelConfTypeDef sConfig;

  hadc.Instance = ADC1;
  hadc.Init.OversamplingMode = DISABLE;
  hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV1;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.SamplingTime = ADC_SAMPLETIME_3CYCLES_5;
  hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ContinuousConvMode = DISABLE;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc.Init.DMAContinuousRequests = DISABLE;
  hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.LowPowerFrequencyMode = DISABLE;
  hadc.Init.LowPowerAutoPowerOff = DISABLE;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  


}

/*
static void MX_IWDG_Init(void)
{

  hiwdg.Instance = IWDG;
  hiwdg.Init.Prescaler = IWDG_PRESCALER_8;
  hiwdg.Init.Window = 4095;
  hiwdg.Init.Reload = 4095;
  if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}
*/

static void MX_LPTIM1_Init(void)
{

  hlptim1.Instance = LPTIM1;
  hlptim1.Init.Clock.Source = LPTIM_CLOCKSOURCE_APBCLOCK_LPOSC;
  hlptim1.Init.Clock.Prescaler = LPTIM_PRESCALER_DIV1;
  hlptim1.Init.Trigger.Source = LPTIM_TRIGSOURCE_SOFTWARE;
  hlptim1.Init.OutputPolarity = LPTIM_OUTPUTPOLARITY_HIGH;
  hlptim1.Init.UpdateMode = LPTIM_UPDATE_IMMEDIATE;
  hlptim1.Init.CounterSource = LPTIM_COUNTERSOURCE_INTERNAL;
  if (HAL_LPTIM_Init(&hlptim1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}


static void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}


static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  
  HAL_GPIO_WritePin(GPIOA, BTEN_Pin|SESEN_Pin|GPIO_PIN_15, GPIO_PIN_RESET);

 
  GPIO_InitStruct.Pin = BTEN_Pin|SESEN_Pin|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  
  GPIO_InitStruct.Pin = USBIN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USBIN_GPIO_Port, &GPIO_InitStruct);

}

void enter_Run( void ) 	
{ 
	/* Enable Clocks */
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	
	/* Force the regulator into main mode */
	// Reset LPRUN bit
	PWR->CR &= ~( PWR_CR_LPRUN );
	// LPSDSR can be reset only when LPRUN bit = 0;
	PWR->CR &= ~( PWR_CR_LPSDSR );

	/* Set HSI16 oscillator as system clock */
	//   Config_SysClk_HSI16();
	    /* Enable Clock */
	    RCC->CR |= RCC_CR_HSION;	// HSI16 oscillator ON
	    while ( !( RCC->CR & RCC_CR_HSIRDY ) ); // wait until HSI16 is ready
	
	    /* Switch System Clock */
	    // HSI16 oscillator used as system clock
    	RCC->CFGR = ( RCC->CFGR & ~RCC_CFGR_SW ) | RCC_CFGR_SW_HSI;
    	while ( ( RCC->CFGR & RCC_CFGR_SWS ) != RCC_CFGR_SWS_HSI ); // wait until switched

	    /* Disable other clocks (excluding LSE and LSI) */
	    RCC->CR &= ~( RCC_CR_MSION | RCC_CR_HSEON | RCC_CR_PLLON );

	SystemCoreClockUpdate();
	// Reinitialize peripherals dependent on clock speed
	
}





void enter_LPRun( void )
{
	
	/* 1. Each digital IP clock must be enabled or disabled by using the
	 	 	 	RCC_APBxENR and RCC_AHBENR registers */
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;

	/* 2. The frequency of the system clock must be decreased to not exceed the
	      frequency of f_MSI range1. */


	     //     Config_SysClk_MSI_131();
        /* Enable and Configure Clock */
	    RCC->CR |= RCC_CR_MSION;
	    RCC->ICSCR = ( RCC->ICSCR & ~RCC_ICSCR_MSIRANGE ) | RCC_ICSCR_MSIRANGE_1;
	    while ( !( RCC->CR & RCC_CR_MSIRDY ) ); // wait until MSI is ready
	    
	    /* Switch System Clock */
	    // MSI oscillator used as system clock
	    RCC->CFGR = ( RCC->CFGR & ~RCC_CFGR_SW ) | RCC_CFGR_SW_MSI;
	    while ( ( RCC->CFGR & RCC_CFGR_SWS ) != RCC_CFGR_SWS_MSI ); // wait unit switched
	
	    /* Disable other clocks (excluding LSE and LSI) */
	    RCC->CR &= ~( RCC_CR_HSION | RCC_CR_HSEON | RCC_CR_PLLON );
	
	SystemCoreClockUpdate();
	
	//MX_LPTIM1_Init();
 // MX_IWDG_Init();

 
	/*// Reinitialize peripherals dependent on clock speed
	 3. The regulator is forced in low-power mode by software
	      (LPRUN and LPSDSR bits set ) */
	PWR->CR &= ~PWR_CR_LPRUN; // Be sure LPRUN is cleared!	
	PWR->CR |= PWR_CR_LPSDSR; // must be set before LPRUN
	PWR->CR |= PWR_CR_LPRUN; // enter low power run mode
}



void enter_Sleep( void )
{
		
	/* Configure low-power mode */
	SCB->SCR &= ~( SCB_SCR_SLEEPDEEP_Msk );  // low-power mode = sleep mode
	SCB->SCR |= SCB_SCR_SLEEPONEXIT_Msk;     // reenter low-power mode after ISR
	
	/* Ensure Flash memory stays on */
	FLASH->ACR &= ~FLASH_ACR_SLEEP_PD;

	__WFI();  // enter low-power mode
}



void Delay(uint32_t Dely)
{
	 	HAL_LPTIM_Counter_Start_IT(&hlptim1, 0xFFFF); //value between 0x0000 and 0xFFFF.		

  while(((uint32_t) HAL_LPTIM_ReadCounter (&hlptim1))/1000 <Dely)
  {
		
  }
	HAL_LPTIM_Counter_Stop(&hlptim1);
}
/*void v_bat()
{	
	      uint32_t voltage;
	
	      ADC_ChannelConfTypeDef sConfig;
	  	  sConfig.Channel = ADC_CHANNEL_1; 
	      sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;   		
		    HAL_ADC_ConfigChannel(&hadc,  &sConfig);		
        HAL_ADC_Start(&hadc);
        HAL_ADC_PollForConversion(&hadc,100);
        voltage = (uint32_t) HAL_ADC_GetValue(&hadc);
	   	  HAL_ADC_Stop(&hadc);
		    HAL_Delay(100);
	    	sprintf(strADC,"%04d",voltage);				
				HAL_UART_Transmit(&huart1,strUART_DEV_ID,10,0xFFFF);
			  HAL_UART_Transmit(&huart1,str_USART_bat_level,10,0xFFFF);	  	
		    HAL_UART_Transmit(&huart1,strADC,8,0xFFFF);
	      HAL_UART_Transmit(&huart1,str_USART_new_line,2,0xFFFF);
	   
	
	
}
*/
/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

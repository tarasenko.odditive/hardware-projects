#include "main.h"
#include "stm32l0xx_hal.h"
#include "string.h"


ADC_HandleTypeDef hadc;
IWDG_HandleTypeDef hiwdg;
LPTIM_HandleTypeDef hlptim1;
UART_HandleTypeDef huart2;


void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC_Init(void);
static void MX_IWDG_Init(void);
static void MX_LPTIM1_Init(void);
static void MX_USART2_UART_Init(void);




uint8_t str[]="USART Transmit\r\n";
char strADC[8]={0};
uint16_t iADC;
	 	 	 
  


int main(void)
{
 
	
		
	
	
  HAL_Init();
  SystemClock_Config();
  MX_GPIO_Init();
  MX_ADC_Init();
  MX_IWDG_Init();
  MX_LPTIM1_Init();
  MX_USART2_UART_Init();
	
	
	
	 

  while (1)
  {

	HAL_UART_Transmit(&huart2,str,8,0xFFFF);
  HAL_Delay(200);
  }


}


void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_LPTIM1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_SYSCLK;
  PeriphClkInit.LptimClockSelection = RCC_LPTIM1CLKSOURCE_LSI;

  

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}
static void MX_ADC_Init(void)
{
 ADC_ChannelConfTypeDef sConfig;
  hadc.Instance = ADC1;
  hadc.Init.OversamplingMode = DISABLE;
  hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc.Init.Resolution = ADC_RESOLUTION_10B;
  hadc.Init.SamplingTime = ADC_SAMPLETIME_3CYCLES_5;
  hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ContinuousConvMode = DISABLE;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc.Init.DMAContinuousRequests = DISABLE;
  hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.LowPowerFrequencyMode = DISABLE;
  hadc.Init.LowPowerAutoPowerOff = DISABLE;
  
 /**Configure for the selected ADC regular channel to be converted.*/

  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
 
  
  sConfig.Channel = ADC_CHANNEL_2;
 

}





static void MX_IWDG_Init(void)
{

  hiwdg.Instance = IWDG;
  hiwdg.Init.Prescaler = IWDG_PRESCALER_4;
  hiwdg.Init.Window = 4095;
  hiwdg.Init.Reload = 4095;
 
}

static void MX_LPTIM1_Init(void)
{

  hlptim1.Instance = LPTIM1;
  hlptim1.Init.Clock.Source = LPTIM_CLOCKSOURCE_APBCLOCK_LPOSC;
  hlptim1.Init.Clock.Prescaler = LPTIM_PRESCALER_DIV1;
  hlptim1.Init.Trigger.Source = LPTIM_TRIGSOURCE_SOFTWARE;
  hlptim1.Init.OutputPolarity = LPTIM_OUTPUTPOLARITY_HIGH;
  hlptim1.Init.UpdateMode = LPTIM_UPDATE_IMMEDIATE;
  hlptim1.Init.CounterSource = LPTIM_COUNTERSOURCE_INTERNAL;
  
}


static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
 
}
static void MX_GPIO_Init(void)	
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, BT_sw_Pin|Gas_sens_sw_Pin|BT_rst_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : BT_sw_Pin Gas_sens_sw_Pin BT_rst_Pin */
  GPIO_InitStruct.Pin = BT_sw_Pin|Gas_sens_sw_Pin|BT_rst_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_in_ST_Pin */
  GPIO_InitStruct.Pin = USB_in_ST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(USB_in_ST_GPIO_Port, &GPIO_InitStruct);

}


void enter_Run( void ) 	
{ 
	/* Enable Clocks */
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	
	/* Force the regulator into main mode */
	// Reset LPRUN bit
	PWR->CR &= ~( PWR_CR_LPRUN );
	// LPSDSR can be reset only when LPRUN bit = 0;
	PWR->CR &= ~( PWR_CR_LPSDSR );

	/* Set HSI16 oscillator as system clock */
	//   Config_SysClk_HSI16();
	    /* Enable Clock */
	    RCC->CR |= RCC_CR_HSION;	// HSI16 oscillator ON
	    while ( !( RCC->CR & RCC_CR_HSIRDY ) ); // wait until HSI16 is ready
	
	    /* Switch System Clock */
	    // HSI16 oscillator used as system clock
    	RCC->CFGR = ( RCC->CFGR & ~RCC_CFGR_SW ) | RCC_CFGR_SW_HSI;
    	while ( ( RCC->CFGR & RCC_CFGR_SWS ) != RCC_CFGR_SWS_HSI ); // wait until switched

	    /* Disable other clocks (excluding LSE and LSI) */
	    RCC->CR &= ~( RCC_CR_MSION | RCC_CR_HSEON | RCC_CR_PLLON );

	SystemCoreClockUpdate();
	// Reinitialize peripherals dependent on clock speed
	   MX_GPIO_Init();
     MX_ADC_Init();
     MX_IWDG_Init();
     MX_LPTIM1_Init();
     MX_USART2_UART_Init();
}




void enter_LPRun( void )
{
	
	/* 1. Each digital IP clock must be enabled or disabled by using the
	 	 	 	RCC_APBxENR and RCC_AHBENR registers */
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;

	/* 2. The frequency of the system clock must be decreased to not exceed the
	      frequency of f_MSI range1. */


	     //     Config_SysClk_MSI_131();
        /* Enable and Configure Clock */
	    RCC->CR |= RCC_CR_MSION;
	    RCC->ICSCR = ( RCC->ICSCR & ~RCC_ICSCR_MSIRANGE ) | RCC_ICSCR_MSIRANGE_1;
	    while ( !( RCC->CR & RCC_CR_MSIRDY ) ); // wait until MSI is ready
	    
	    /* Switch System Clock */
	    // MSI oscillator used as system clock
	    RCC->CFGR = ( RCC->CFGR & ~RCC_CFGR_SW ) | RCC_CFGR_SW_MSI;
	    while ( ( RCC->CFGR & RCC_CFGR_SWS ) != RCC_CFGR_SWS_MSI ); // wait unit switched
	
	    /* Disable other clocks (excluding LSE and LSI) */
	    RCC->CR &= ~( RCC_CR_HSION | RCC_CR_HSEON | RCC_CR_PLLON );
	
	SystemCoreClockUpdate();
	// Reinitialize peripherals dependent on clock speed
	  MX_GPIO_Init();
    MX_ADC_Init();
    MX_IWDG_Init();
    MX_LPTIM1_Init();
    MX_USART2_UART_Init();
	/* 3. The regulator is forced in low-power mode by software
	      (LPRUN and LPSDSR bits set ) */
	PWR->CR &= ~PWR_CR_LPRUN; // Be sure LPRUN is cleared!
	
	PWR->CR |= PWR_CR_LPSDSR; // must be set before LPRUN
	PWR->CR |= PWR_CR_LPRUN; // enter low power run mode
}



void enter_Sleep( void )
{
	
	/* Configure low-power mode */
	SCB->SCR &= ~( SCB_SCR_SLEEPDEEP_Msk );  // low-power mode = sleep mode
	SCB->SCR |= SCB_SCR_SLEEPONEXIT_Msk;     // reenter low-power mode after ISR
	
	/* Ensure Flash memory stays on */
	FLASH->ACR &= ~FLASH_ACR_SLEEP_PD;

	__WFI();  // enter low-power mode
}

void ADC_read(void)
{
	 
	 
        HAL_ADC_Start(&hadc);
        HAL_ADC_PollForConversion(&hadc,100);
        iADC = HAL_ADC_GetValue(&hadc);
	    	HAL_Delay(500);
	      
} 
